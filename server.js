/**
 * This is my second nodejs-express demo project
 * Author TuanLA27
 */

// Call needed packages
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

// Configure app using body-parser
// This will let we get data from a POST method
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Set PORT to run service API, use PORT property in process environment or set it directly
let port = process.env.PORT || 2707;

// CONNECT TO DATABASE (Mongodb)
//=================================================================
mongoose.connect('mongodb://127.0.0.1:27017/tuanla'); // connect to database name is tuanla

// Setup person table(Schema)
let Person = require('./model/person');

// API C.O.R.S
//=================================================================
// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.header('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.header('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

// ROUTER SECTION
//=================================================================
let router = express.Router();

// set default route when load page in first time
router.get('/', function (req, res) {
  res.json({message: 'This is TuanLA27 API'});
});

// REGISTER OUR ROUTERS
//=================================================================
app.use('/', router);
app.use('/api', router);

// middleware use for all request
router.use(function (req, res, next) {
  console.log('Service is running!');
  next();
});

// Setup routes that end with /persons
router.route('/persons')
  // create new person in database
  .post(function (req, res) {
    let person = new Person();
    person.name = req.body.name;
    person.edited = req.body.edited;
    person.company = req.body.company;

    // save person and check error
    person.save(function (error) {
      if (error) {
        res.send(error);
      }
      res.json({message: 'Create Successfully!'});
    })
  })

  .get(function (req, res) {
    Person.find(function (error, persons) {
      if (error) {
        res.send(error);
      }
      res.json(persons);
    })
  })

router.route('/person/:id')
  .put(function (req, res) {
    Person.findById(req.params.id, function (error, person) {
      if (error) {
        res.send(error)
      }
      person.name = req.body.name;
      person.company = req.body.company;
      person.edited = req.body.edited;
      person.save(function (error) {
        if (error) {
          res.send(error);
        }
        res.json({message: 'Update Successfully!'})
      })
    })
  })
  .get(function (req, res) {
    Person.findById(req.params.id, function(error, person) {
       if (error) {
         res.send(error);
       }
       res.json(person);
    })
  })
  .delete(function (req, res) {
    Person.remove({_id: req.params.id}, function (error) {
      if (error) {
        res.send(error)
      }
      res.json({message: 'Deleted Successfully!'});
    })
  })

// START THE SERVER
//=================================================================
app.listen(port);
console.log('TuanLA27 API is now running!');