let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let personSchema = new Schema({
  name: String,
  company: String,
  edited: Boolean
});

module.exports = mongoose.model('Person', personSchema);